Ciclos

Los ciclos en Ansible nos van a permitir iterar en un conjunto de datos. Para
este ejemplo definiremos en un arreglo los nombres de diferentes archivos que
vamos a copiar a nuestro servidor remoto.

Para hacer esto debemos irnos al módulo de archivos en el apartado de file y
usaremos el parámetro state que será touch, el src, el dest y el path.
https://docs.ansible.com/ansible/latest/modules/file_module.html#file-module

Cuando hacemos un loop en Python, siempre asignamos un nombre a la variable.
En este caso, la variable por defecto se llama item (No se puede usar por
ahora otro nombre).

Si ejecutamos el comando
ansible-playbook nginx.yml -C
podemos observar lo siguiente:
PLAY [web] ************************************************************************************************************

TASK [Gathering Facts] ************************************************************************************************
ok: [10.0.0.200]
ok: [10.0.0.100]

TASK [debug] **********************************************************************************************************
ok: [10.0.0.100] => {
    "ansible_all_ipv6_addresses": [
        "fe80::a00:27ff:fe5f:bbe6",
        "fe80::a00:27ff:fe6a:ab75"
    ]
}
ok: [10.0.0.200] => {
    "ansible_all_ipv6_addresses": [
        "fe80::a00:27ff:fe5f:bbe6",
        "fe80::a00:27ff:fe45:1d33"
    ]
}

TASK [Create files] ***************************************************************************************************
ok: [10.0.0.100] => (item=uno)
ok: [10.0.0.200] => (item=uno)
ok: [10.0.0.100] => (item=dos)
ok: [10.0.0.200] => (item=dos)
ok: [10.0.0.100] => (item=tres)
ok: [10.0.0.200] => (item=tres)

PLAY RECAP ************************************************************************************************************
10.0.0.100                 : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.0.200                 : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

Ahora ejecutamos el comando sin el flag -C. En los servidores podemos ver
que se crearon de forma satisfactoria los archivos uno, dos y tres.

Si hubiesemos definido un pequeño diccionario de elementos clave-valor, 
tendríamos que usar la variable item seguido con un punto y la propiedad,
por ejemplo item.name.

Otra cosa que podemos agregar en este apartado es establecer una pequeña 
condicional con el parámetro when.
PLAY [web] ************************************************************************************************************

TASK [Gathering Facts] ************************************************************************************************
ok: [10.0.0.200]
ok: [10.0.0.100]

TASK [debug] **********************************************************************************************************
ok: [10.0.0.100] => {
    "ansible_all_ipv6_addresses": [
        "fe80::a00:27ff:fe5f:bbe6",
        "fe80::a00:27ff:fe6a:ab75"
    ]
}
ok: [10.0.0.200] => {
    "ansible_all_ipv6_addresses": [
        "fe80::a00:27ff:fe5f:bbe6",
        "fe80::a00:27ff:fe45:1d33"
    ]
}

TASK [Create files] ***************************************************************************************************
ok: [10.0.0.100] => (item=uno)
skipping: [10.0.0.100] => (item=dos)
skipping: [10.0.0.100] => (item=tres)
ok: [10.0.0.200] => (item=uno)
skipping: [10.0.0.200] => (item=dos)
skipping: [10.0.0.200] => (item=tres)

PLAY RECAP ************************************************************************************************************
10.0.0.100                 : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.0.0.200                 : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

Notese que el archivo uno se copia pero el dos y el tres se saltan por la 
condicional.