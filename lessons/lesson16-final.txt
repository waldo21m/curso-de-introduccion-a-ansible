Final

En esta última lección, vamos a configurar un servidor físico. En el curso
usarán una cuenta en DO, pero nosotros podremos usar un servidor con Vagrant
siguiente las mismas indicaciones.

Para ello vamos a usar la referencia indicada en la página
https://www.digitalocean.com/community/tutorials/configuracion-inicial-del-servidor-con-el-sistema-ubuntu-18-04-es

Para este ejemplo necesitamos tener un droplet (vps o máquina virtual) y tener
a la mano el siguiente tutorial:
https://codigofacilito.com/articulos/autenticacion_servidores_remotos

Lo que debemos hacer es copiar nuestra llave ssh a nuestro servidor para que
nos podamos autenticar y para ello ejecutamos:

ssh-copy-id -i ~/.ssh/id_rsa.pub user@ip_address

Si todo salió de forma exitosa, podemos ingresar a nuestra máquina con el 
comando

ssh -i ~/.ssh/id_rsa root@10.0.0.100

(Esto no va a funcionar en vagrant porque él copia la llave ssh de forma
automática pero te pedirá la clave en todo momento. Para evitar eso, podemos
usar la configuración de vagrant que vimos en el curso)

config.ssh.insert_key = false
config.ssh.private_key_path = ["~/.ssh/id_rsa", "~/.vagrant.d/insecure_private_key"]
config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "~/.ssh/authorized_keys"

Una vez que ya pudimos ingresar vamos a modificar el archivo inventory 
(ver lesson16/hosts)

Nota: El usuario remoto y la llave privada se puede definir tanto en el 
inventory como en el archivo ansible.cfg

Podemos verificar nuestra configuración del inventory con el comando

ansible -m ping example

Ahora crearemos un playbook llamado example.yml donde estableceremos los hosts,
el usuario remoto, un usuario que se llama sammy (por la documentación indicada
en DigitalOcean), una copia de la llave local que irá a nuestro servidor remoto.

Según la guía de DO, debemos crear un usuario y otorgarle permisos de super
usuario y esto lo podemos hacer con el módulo de user de ansible:
https://docs.ansible.com/ansible/latest/modules/user_module.html
De esta forma estaríamos logrando los pasos 2 y 3. El paso 4 se obviará por
los momentos.

Para el paso #5 debemos copiar la llave ssh a nuestro servidor remoto y para
ello nos apoyaremos del módulo authorized_key la cuál nos permite agregar o
quitar llaves ssh de cuentas de usuarios particulares.
https://docs.ansible.com/ansible/latest/modules/authorized_key_module.html

Ahora usaremos el módulo apt para actualizar e instalar nuestros paquetes
https://docs.ansible.com/ansible/latest/modules/apt_module.html

Nota: Se puede usar varios parámetros en la misma línea se la siguiente 
forma:
    - name: Install packages
      apt: name="{{ packages }}" state=latest

Es equivalente a esto:
    - name: Install packages
      apt: 
        name: "{{ packages }}"
        state: latest

No hace falta iterar los paquetes con un ciclo for.

Ahora procedemos a usar el módulo de ufw de ansible
https://docs.ansible.com/ansible/latest/modules/ufw_module.html
Este es un páquete oficial que nos permite interactuar con nuestro firewall 
ufw.

Siguiendo el paso 4 de la documentación de DO, debemos permitir la
conexión a ciertos servicios y es permitir acceder al puerto 22 para
conexiones mediante la llave ssh (Con OpenSSH). Y vamos a denegar
todo el tráfico que este llegando a nuestro firewall.

Recapitulando, lo que hicimos en example.yml fue establecer el host example,
el usuario remoto (vagrant en este caso), definimos una serie de variables
que usaremos en los tasks y establecimos las siguientes tareas: Crear un
usuario, agregamos la llave ssh, actualizamos e instalamos nuevos paquetes
y con el firewall permitimos el puerto 22 y bloqueamos todos los demas.

Para probar nuestro playbook ejecutamos el comando:

ansible-playbook example.yml

Lo mejor es hacer esto con una máquina real y no con una máquina virtual porque
va a petar por todas partes.